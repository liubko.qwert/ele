'use strict';
var express    = require('express');
var app        = express();
var router     = express.Router();
var bodyParser = require('body-parser');

app.use(bodyParser.json({limit:1024*1024}));
app.use(bodyParser.urlencoded());
app.use(express.static( __dirname + '/../client/public/'));
app.use(router);

app.listen(5000);
module.exports = app;