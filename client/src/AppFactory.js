'use strict';

var App = require('./App');
var Pubsub = require('pubsub-js');
var Repository = require('./Repository');
var Presenter = require('./Presenter');
var UI = require('./ui/UI.jsx');

var ui = UI({
    pubsub: Pubsub
});

var repository = new Repository();

var presenter = new Presenter({
    repository: repository,
    ui: ui,
    pubsub: Pubsub
});

class AppFactory {
    create() {
        return new App({
            pubsub: Pubsub,
            repository: repository,
            ui: ui,
            presenter: presenter
        });
    }
}

module.exports = AppFactory;