/**
 * @jsx React.DOM
 */
'use strict';

var React = require('react');
var _ = require('lodash');
var Jumbotron = require('./../components/Jumbotron.jsx');
var TopicListView = require('./../components/TopicListView.jsx');

var HomePage = React.createClass({
    propTypes: {
        data: React.PropTypes.object.isRequired,
        pubsub: React.PropTypes.object.isRequired,
    },

    handleSelectTopic(topicID) {
        this.props.pubsub.publish('selectTopic',{
            topicID: topicID
        });
    },

    render() {
        var rows = [];
        var threeTopics = [];
        _.each(this.props.data.topics, (topic, index) => {
            console.log(index, rows, threeTopics);
            threeTopics.push(
                <TopicListView  key={index}
                                showBtn={typeof topic.id !== 'undefined'}
                                title={topic.name}
                                description={topic.description}
                                quizResult={topic.quizResult}
                                onClick={this.handleSelectTopic.bind(this, topic.id)} />
            );

            if (threeTopics.length===3) {
                var tmp = threeTopics.slice(0);
                rows.push(<div className="row"> {tmp} </div>);
                threeTopics.length = 0;
            }
        });
        rows.push(<div className="row"> {threeTopics} </div>);

        return (
            <div className="HomePage">
                <Jumbotron onClick={this.handleStartExam}/>

                <div className="container">
                    {rows}
                </div>
            </div>
        );
    }
});

module.exports = HomePage;