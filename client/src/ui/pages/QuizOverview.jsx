/**
 * @jsx React.DOM
 */
'use strict';

var React = require('react');
var cx = require('react-classset');
var _ = require('lodash');
var Button = require('../components/Button.jsx');
// require('./Header.css');

var QuizOverview = React.createClass({
    propTypes: {
        data: React.PropTypes.object.isRequired,
        pubsub: React.PropTypes.object.isRequired,
    },

    handleGoHome() {
        this.props.pubsub.publish('showHomePage');
    },

    render() {
        var quizResult = this.props.data.quizResult;
        var questionsNode = _.map(quizResult, (data, index) => {
            var panelClass = cx({
                'panel': true,
                'panel-success': data.isRight,
                'panel-danger': !data.isRight
            });

            var userAnswerNode;
            var explanationNode;
            if (!data.isRight) {
                userAnswerNode = (<p><strong>Your answer </strong>{data.userAnswer}</p>);
                explanationNode = (<blockquote><p>{data.answerExplanation}</p></blockquote>);
            }

            return (
                <div className={panelClass}>
                    <div className="panel-heading">
                        <h3 className="panel-title">Question №{index+1}</h3>
                    </div>
                    <div className="panel-body">
                        <h4>{data.question}</h4>
                        {userAnswerNode}
                        <p><strong>Right answer </strong>{data.rightAnswer}</p>
                        {explanationNode}
                    </div>
                </div>
            );
        });


        var totalCountRightAnswers = _.where(quizResult, {isRight: true}).length;
        var totalCountQuestions = quizResult.length;
        var totalResult = totalCountRightAnswers + '/' + totalCountQuestions;
        return (
            <div className="QuizOverview container">
                <h1>Your quiz result is {totalResult}</h1>
                {questionsNode}

                <div className="row">
                    <Button value="Back to main"
                            type="primary"
                            size="large"
                            onClick={this.handleGoHome} />
                </div>
            </div>
        );
    }
});

module.exports = QuizOverview;