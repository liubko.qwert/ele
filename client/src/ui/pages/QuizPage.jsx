/**
 * @jsx React.DOM
 */
'use strict';

var React = require('react');
var _ = require('lodash');
var Button = require('../components/Button.jsx');
var Question = require('../components/Question.jsx');
require('./QuizPage.css');

var QuizPage = React.createClass({
    propTypes: {
        data: React.PropTypes.object.isRequired,
        pubsub: React.PropTypes.object.isRequired,
    },

    handleSubmitAnswers() {
        var answers = _.map(this.props.data.quizData.questions, (question, index) => {
            return {
                questionID: question.id,
                answer: this.refs['question_'+index].getAnswer()
            };
        });

        this.props.pubsub.publish('submitAnswers', {
            topicID: this.props.data.topicID,
            userAnswers: answers
        });
    },

    render() {
        var quizData = this.props.data.quizData;
        var questionsNode = _.map(quizData.questions, (question, index) =>
            <Question ref={'question_'+index} data={question} key={index}/>
        );

        return (
            <div className="QuizPage container">
                <h1>{quizData.name}</h1>
                <h4>{quizData.description}</h4>
                <ul>
                    {questionsNode}
                </ul>
                <div className="row">
                    <Button value="Submit answers"
                            type="primary"
                            size="large"
                            onClick={this.handleSubmitAnswers} />
                </div>
            </div>
        );
    }
});

module.exports = QuizPage;