/**
 * @jsx React.DOM
 */
'use strict';

var React = require('react');
var TopicPageWrap = require('./TopicPageWrap.jsx');
var CodeExample = require('./../components/CodeExample.jsx');

var Topic = React.createClass({
    propTypes: {
        data: React.PropTypes.object.isRequired,
        pubsub: React.PropTypes.object.isRequired,
    },

    render() {
        return (
            <TopicPageWrap topicID="topic-1" pubsub={this.props.pubsub}>
                <h1>Introduction</h1>

                <p>ECMAScript 6 is the upcoming version of the ECMAScript standard. This standard is targeting ratification in December 2014. ES6 is a significant update to the language, and the first update to the language since ES5 was standardized in 2009. Implementation of these features in major JavaScript engines is underway now.</p>

                <p>ES6 includes the following new features:</p>

                <ul>
                    <li>arrows</li>
                    <li>classes</li>
                    <li>default + rest + spread</li>
                    <li>enhanced object literals</li>
                    <li>template strings</li>
                    <li>let + const</li>
                    <li>promises</li>
                    <li>and much more...</li>
                </ul>


                <h2>Arrows</h2>
                <p>{'Arrows are a function shorthand using the => syntax. They are syntactically similar to the related feature in C#, Java 8 and CoffeeScript. They support both expression and statement bodies. Unlike functions, arrows share the same lexical this as their surrounding code.'}</p>
                <p>{'Arrow functions are different than traditional functions but do share some common characteristics. For example:'}</p>
                <ul>
                    <li>The typeof operator returns "function" for arrow functions.</li>
                    <li>Arrow functions are still instances of Function, so instanceof works the same way.</li>
                    <li>The methods call(), apply(), and bind() are still usable with arrow functions, though they do not augment the value of this.</li>
                    <li>The biggest difference is that arrow functions can’t be used with new, attempting to do results in an error being thrown.</li>
                </ul>
                <CodeExample data = {
                      '// Expression bodies' + '\n'
                    + 'var odds = evens.map(v => v + 1);' + '\n'
                    + 'var nums = evens.map((v, i) => v + i);' + '\n'
                    + '' + '\n'
                    + '// Statement bodies' + '\n'
                    + 'nums.forEach(v => {' + '\n'
                    + '  if (v % 5 === 0)' + '\n'
                    + '    fives.push(v);' + '\n'
                    + '});' + '\n'
                    + '' + '\n'
                    + '// Lexical this' + '\n'
                    + 'var bob = {' + '\n'
                    + '  _name: "Bob",' + '\n'
                    + '  _friends: [],' + '\n'
                    + '  printFriends() {' + '\n'
                    + '    this._friends.forEach(f =>' + '\n'
                    + '      console.log(this._name + " knows " + f));' + '\n'
                    + '  }' + '\n'
                    + '}' + '\n'
                } />

                <h2>Classes</h2>
                <p>{'ES6 classes are a simple sugar over the prototype-based OO pattern. Having a single convenient declarative form makes class patterns easier to use, and encourages interoperability. Classes support prototype-based inheritance, super calls, instance and static methods and constructors.'}</p>
                <CodeExample data = {
                    'class SkinnedMesh extends THREE.Mesh {' + '\n'
                    + '  constructor(geometry, materials) {' + '\n'
                    + '    super(geometry, materials);' + '\n'
                    + '' + '\n'
                    + '    this.idMatrix = SkinnedMesh.defaultMatrix();' + '\n'
                    + '    this.bones = [];' + '\n'
                    + '    this.boneMatrices = [];' + '\n'
                    + '    //...' + '\n'
                    + '  }' + '\n'
                    + '  update(camera) {' + '\n'
                    + '    //...' + '\n'
                    + '    super.update();' + '\n'
                    + '  }' + '\n'
                    + '  static defaultMatrix() {' + '\n'
                    + '    return new THREE.Matrix4();' + '\n'
                    + '  }' + '\n'
                    + '}' + '\n'
                } />

                <h2>Default + Rest + Spread</h2>
                <p>{'Callee-evaluated default parameter values. Turn an array into consecutive arguments in a function call. Bind trailing parameters to an array. Rest replaces the need for arguments and addresses common cases more directly.'}</p>
                <CodeExample data = {
                      'function f(x, y=12) {' + '\n'
                    + '  // y is 12 if not passed (or passed as undefined)' + '\n'
                    + '  return x + y;' + '\n'
                    + '}' + '\n'
                    + 'f(3) == 15' + '\n'
                    + 'function f(x, ...y) {' + '\n'
                    + '  // y is an Array' + '\n'
                    + '  return x * y.length;' + '\n'
                    + '}' + '\n'
                    + 'f(3, "hello", true) == 6' + '\n'
                    + 'function f(x, y, z) {' + '\n'
                    + '  return x + y + z;' + '\n'
                    + '}' + '\n'
                    + '// Pass each elem of array as argument' + '\n'
                    + 'f(...[1,2,3]) == 6' + '\n'
                } />

                <h2>Enhanced Object Literals</h2>
                <p>{'Object literals are extended to support setting the prototype at construction, shorthand for foo: foo assignments, defining methods and making super calls. Together, these also bring object literals and class declarations closer together, and let object-based design benefit from some of the same conveniences.'}</p>
                <CodeExample data = {
                      'var obj = {' + '\n'
                    + '    // __proto__' + '\n'
                    + '    __proto__: theProtoObj,' + '\n'
                    + '    // Shorthand for ‘handler: handler’' + '\n'
                    + '    handler,' + '\n'
                    + '    // Methods' + '\n'
                    + '    toString() {' + '\n'
                    + '     // Super calls' + '\n'
                    + '     return "d " + super.toString();' + '\n'
                    + '    },' + '\n'
                    + '    // Computed (dynamic) property names' + '\n'
                    + '    [ "prop_" + (() => 42)() ]: 42' + '\n'
                    + '};' + '\n'
                } />

                <h2>Template Strings</h2>
                <p>{'Template strings provide syntactic sugar for constructing strings. This is similar to string interpolation features in Perl, Python and more. Optionally, a tag can be added to allow the string construction to be customized, avoiding injection attacks or constructing higher level data structures from string contents.'}</p>
                <CodeExample data = {
                      "// Basic literal string creation" + "\n"
                    + "`In JavaScript \\n is a line-feed.`" + "\n"
                    + "" + "\n"
                    + "// Multiline strings" + "\n"
                    + "`In JavaScript this is" + "\n"
                    + " not legal.`" + "\n"
                    + "" + "\n"
                    + "// Construct a DOM query" + "\n"
                    + "var name = 'Bob', time = 'today';" + "\n"
                    + "`Hello ${name}, how are you ${time}?`" + "\n"
                } />


                <h2>Let + Const</h2>
                <p>{'Block-scoped binding constructs. let is the new var. const is single-assignment. Static restrictions prevent use before assignment.'}</p>
                <CodeExample data = {
                      'function f() {' + '\n'
                    + '  {' + '\n'
                    + '    let x;' + '\n'
                    + '    {' + '\n'
                    + '      // okay, block scoped name' + '\n'
                    + '      const x = "sneaky";' + '\n'
                    + '      // error, const' + '\n'
                    + '      x = "foo";' + '\n'
                    + '    }' + '\n'
                    + '    // error, already declared in block' + '\n'
                    + '    let x = "inner";' + '\n'
                    + '  }' + '\n'
                    + '}' + '\n'
                } />

                <h2>Promises</h2>
                <p>{'Promises are a library for asynchronous programming. Promises are a first class representation of a value that may be made available in the future. Promises are used in many existing JavaScript libraries.'}</p>
                <CodeExample data = {
                      'function timeout(duration = 0) {' + '\n'
                    + '    return new Promise((resolve, reject) => {' + '\n'
                    + '        setTimeout(resolve, duration);' + '\n'
                    + '    })' + '\n'
                    + '}' + '\n'
                    + '' + '\n'
                    + 'var p = timeout(1000).then(() => {' + '\n'
                    + '    return timeout(2000);' + '\n'
                    + '}).then(() => {' + '\n'
                    + '    throw new Error("hmm");' + '\n'
                    + '}).catch(err => {' + '\n'
                    + '    return Promise.all([timeout(100), timeout(200)]);' + '\n'
                    + '})' + '\n'
                } />
            </TopicPageWrap>
        );
    }
});

module.exports = Topic;