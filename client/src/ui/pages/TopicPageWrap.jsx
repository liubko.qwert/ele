/**
 * @jsx React.DOM
 */
'use strict';

var React = require('react');
var Button = require('../components/Button.jsx');

require('./TopicPageWrap.css');

var TopicPageWrap = React.createClass({
    propTypes: {
        topicID: React.PropTypes.string.isRequired,
        pubsub: React.PropTypes.object.isRequired,
    },

    handleStartQuiz() {
        this.props.pubsub.publish('startQuiz', {
            topicID: this.props.topicID
        });
    },

    render() {
        return (
            <div className="TopicPageWrap container">
                <div className="row">
                    {this.props.children}
                </div>

                <div className="row">
                    <Button value="Start Quiz"
                            type="primary"
                            size="large"
                            onClick={this.handleStartQuiz} />
                </div>
            </div>
        );
    }
});

module.exports = TopicPageWrap;