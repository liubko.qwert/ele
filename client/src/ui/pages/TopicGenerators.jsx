/**
 * @jsx React.DOM
 */
'use strict';

var React = require('react');
var TopicPageWrap = require('./TopicPageWrap.jsx');
var CodeExample = require('./../components/CodeExample.jsx');

var Topic = React.createClass({
    propTypes: {
        data: React.PropTypes.object.isRequired,
        pubsub: React.PropTypes.object.isRequired,
    },

    render() {
        return (
            <TopicPageWrap topicID="topic-2" pubsub={this.props.pubsub}>
                <h1>ES6 Generators</h1>

                <p>One of the most exciting new features coming in JavaScript ES6 is a new breed of function, called a generator. The name is a little strange, but the behavior may seem a lot stranger at first glance. This article aims to explain the basics of how they work, and build you up to understanding why they are so powerful for the future of JS.</p>

                <h2>Run-To-Completion</h2>
                <p>{'The first thing to observe as we talk about generators is how they differ from normal functions with respect to the "run to completion" expectation.'}</p>
                <p>{'Whether you realized it or not, you\'ve always been able to assume something fairly fundamental about your functions: once the function starts running, it will always run to completion before any other JS code can run.'}</p>
                <p>{'Example:'}</p>
                <CodeExample data = {
                    'setTimeout(function(){' + '\n'
                    + '    console.log("Hello World");' + '\n'
                    + '},1);' + '\n'
                    + '' + '\n'
                    + 'function foo() {' + '\n'
                    + '    // NOTE: don\'t ever do crazy long-running loops like this' + '\n'
                    + '    for (var i=0; i<=1E10; i++) {' + '\n'
                    + '        console.log(i);' + '\n'
                    + '    }' + '\n'
                    + '}' + '\n'
                    + '' + '\n'
                    + 'foo();' + '\n'
                    + '// 0..1E10' + '\n'
                    + '// "Hello World"' + '\n'
                } />
                <p>{'Here, the for loop will take a fairly long time to complete, well more than one millisecond, but our timer callback with the console.log(..) statement cannot interrupt the foo() function while it\'s running, so it gets stuck at the back of the line (on the event-loop) and it patiently waits its turn.'}</p>
                <p>{'What if foo() could be interrupted, though? Wouldn\'t that cause havoc in our programs?'}</p>
                <p>{'That\'s exactly the nightmares challenges of multi-threaded programming, but we are quite fortunate in JavaScript land to not have to worry about such things, because JS is always single-threaded (only one command/function executing at any given time).'}</p>
                <p>{'Note: Web Workers are a mechanism where you can spin up a whole separate thread for a part of a JS program to run in, totally in parallel to your main JS program thread. The reason this doesn\'t introduce multi-threaded complications into our programs is that the two threads can only communicate with each other through normal async events, which always abide by the event-loop one-at-a-time behavior required by run-to-completion.'}</p>

                <h2>Run..Stop..Run</h2>
                <p>{'With ES6 generators, we have a different kind of function, which may be paused in the middle, one or many times, and resumed later, allowing other code to run during these paused periods.'}</p>
                <p>{'If you\'ve ever read anything about concurrency or threaded programming, you may have seen the term "cooperative", which basically indicates that a process (in our case, a function) itself chooses when it will allow an interruption, so that it can cooperate with other code. This concept is contrasted with "preemptive", which suggests that a process/function could be interrupted against its will.'}</p>
                <p>{'ES6 generator functions are "cooperative" in their concurrency behavior. Inside the generator function body, you use the new yield keyword to pause the function from inside itself. Nothing can pause a generator from the outside; it pauses itself when it comes across a yield.'}</p>
                <p>{'However, once a generator has yield-paused itself, it cannot resume on its own. An external control must be used to restart the generator. We\'ll explain how that happens in just a moment.'}</p>
                <p>{'So, basically, a generator function can stop and be restarted, as many times as you choose. In fact, you can specify a generator function with an infinite loop (like the infamous while (true) { .. }) that essentially never finishes. While that\'s usually madness or a mistake in a normal JS program, with generator functions it\'s perfectly sane and sometimes exactly what you want to do!'}</p>
                <p>{'Even more importantly, this stopping and starting is not just a control on the execution of the generator function, but it also enables 2-way message passing into and out of the generator, as it progresses. With normal functions, you get parameters at the beginning and a return value at the end. With generator functions, you send messages out with each yield, and you send messages back in with each restart.'}</p>



                <h2>Syntax Please!</h2>
                <p>{'Let\'s dig into the syntax of these new and exciting generator functions.'}</p>
                <p>{'First, the new declaration syntax:'}</p>
                <CodeExample data = {
                    'function *foo() {' + '\n'
                    + '    // ..' + '\n'
                    + '}' + '\n'
                } />
                <p>{'Notice the * there? That\'s new and a bit strange looking. To those from some other languages, it may look an awful lot like a function return-value pointer. But don\'t get confused! This is just a way to signal the special generator function type.'}</p>
                <p>{'You\'ve probably seen other articles/documentation which use function* foo(){ } instead of function *foo(){ } (difference in placement of the *). Both are valid, but I\'ve recently decided that I think function *foo() { } is more accurate, so that\'s what I\'m using here.'}</p>
                <p>{'Now, let\'s talk about the contents of our generator functions. Generator functions are just normal JS functions in most respects. There\'s very little new syntax to learn inside the generator function.'}</p>
                <p>{'The main new toy we have to play with, as mentioned above, is the yield keyword. yield ___ is called a "yield expression" (and not a statement) because when we restart the generator, we will send a value back in, and whatever we send in will be the computed result of that yield ___ expression.'}</p>
                <p>{'Example:'}</p>
                <CodeExample data = {
                  'function *foo() {' + '\n'
                + '    var x = 1 + (yield "foo");' + '\n'
                + '    console.log(x);' + '\n'
                + '}' + '\n'
                } />
                <p>{'The yield "foo" expression will send the "foo" string value out when pausing the generator function at that point, and whenever (if ever) the generator is restarted, whatever value is sent in will be the result of that expression, which will then get added to 1 and assigned to the x variable.'}</p>
                <p>{'See the 2-way communication? You send the value "foo" out, pause yourself, and at some point later (could be immediately, could be a long time from now!), the generator will be restarted and will give you a value back. It\'s almost as if the yield keyword is sort of making a request for a value.'}</p>
                <p>{'In any expression location, you can just use yield by itself in the expression/statement, and there\'s an assumed undefined value yielded out. So:'}</p>

                <CodeExample data = {
                  '// note: `foo(..)` here is NOT a generator!!' + '\n'
                + 'function foo(x) {' + '\n'
                + '    console.log("x: " + x);' + '\n'
                + '}' + '\n'
                + '' + '\n'
                + 'function *bar() {' + '\n'
                + '    yield; // just pause' + '\n'
                + '    foo( yield ); // pause waiting for a parameter to pass into `foo(..)`' + '\n'
                + '}' + '\n'
                } />



                <h2>Generator Iterator</h2>
                <p>{'Iterators are a special kind of behavior, a design pattern actually, where we step through an ordered set of values one at a time by calling next(). Imagine for example using an iterator on an array that has five values in it: [1,2,3,4,5]. The first next() call would return 1, the second next() call would return 2, and so on. After all values had been returned, next() would return null or false or otherwise signal to you that you\'ve iterated over all the values in the data container.'}</p>
                <p>{'The way we control generator functions from the outside is to construct and interact with a generator iterator. That sounds a lot more complicated than it really is. Consider this silly example:'}</p>
                <CodeExample data = {
                  'function *foo() {' + '\n'
                + '    yield 1;' + '\n'
                + '    yield 2;' + '\n'
                + '    yield 3;' + '\n'
                + '    yield 4;' + '\n'
                + '    yield 5;' + '\n'
                + '}' + '\n'
                } />
                <p>{'To step through the values of that *foo() generator function, we need an iterator to be constructed. How do we do that? Easy!'}</p>
                <CodeExample data = {
                    'var it = foo();'
                } />
                <p>{'Oh! So, calling the generator function in the normal way doesn\'t actually execute any of its contents.'}</p>
                <p>{'That\'s a little strange to wrap your head around. You also may be tempted to wonder, why isn\'t it var it = new foo(). Shrugs. The whys behind the syntax are complicated and beyond our scope of discussion here.'}</p>
                <p>{'So now, to start iterating on our generator function, we just do:'}</p>
                <CodeExample data = {
                    'var message = it.next();'
                } />
                <p>{'That will give us back our 1 from the yield 1 statment, but that\'s not the only thing we get back.'}</p>
                <CodeExample data = {
                    'console.log(message); // { value:1, done:false }'
                } />
                <p>{'We actually get back an object from each next() call, which has a value property for the yielded-out value, and done is a boolean that indicates if the generator function has fully completed or not.'}</p>
                <p>{'Let\'s keep going with our iteration:'}</p>
                <CodeExample data = {
                      'console.log( it.next() ); // { value:2, done:false }' + '\n'
                    + 'console.log( it.next() ); // { value:3, done:false }' + '\n'
                    + 'console.log( it.next() ); // { value:4, done:false }' + '\n'
                    + 'console.log( it.next() ); // { value:5, done:false }' + '\n'
                } />
                <p>{'Interesting to note, done is still false when we get the value of 5 out. That\'s because technically, the generator function is not complete. We still have to call a final next() call, and if we send in a value, it has to be set as the result of that yield 5 expression. Only then is the generator function complete.'}</p>
                <p>{'So, now:'}</p>
                <CodeExample data = {
                    'console.log( it.next() ); // { value:undefined, done:true }' + '\n'
                } />
                <p>{'So, the final result of our generator function was that we completed the function, but there was no result given (since we\'d already exhausted all the yield ___ statements).'}</p>
                <p>{'You may wonder at this point, can I use return from a generator function, and if I do, does that value get sent out in the value property?'}</p>
                <p>{'Yes...'}</p>
                <CodeExample data = {
                      'function *foo() {' + '\n'
                    + '    yield 1;' + '\n'
                    + '    return 2;' + '\n'
                    + '}' + '\n'
                    + '' + '\n'
                    + 'var it = foo();' + '\n'
                    + '' + '\n'
                    + 'console.log( it.next() ); // { value:1, done:false }' + '\n'
                    + 'console.log( it.next() ); // { value:2, done:true }' + '\n'
                } />
                <p>{' and no....'}</p>
                <p>{'It may not be a good idea to rely on the return value from generators, because when iterating generator functions with for..of loops (see below), the final returned value would be thrown away.'}</p>
                <p>{'For completeness sake, let\'s also take a look at sending messages both into and out of a generator function as we iterate it:'}</p>
                <CodeExample data = {
                      'function *foo(x) {' + '\n'
                    + '    var y = 2 * (yield (x + 1));' + '\n'
                    + '    var z = yield (y / 3);' + '\n'
                    + '    return (x + y + z);' + '\n'
                    + '}' + '\n'
                    + '' + '\n'
                    + 'var it = foo( 5 );' + '\n'
                    + '' + '\n'
                    + '// note: not sending anything into `next()` here' + '\n'
                    + 'console.log( it.next() );       // { value:6, done:false }' + '\n'
                    + 'console.log( it.next( 12 ) );   // { value:8, done:false }' + '\n'
                    + 'console.log( it.next( 13 ) );   // { value:42, done:true }' + '\n'
                } />
                <p>{'You can see that we can still pass in parameters (x in our example) with the initial foo( 5 ) iterator-instantiation call, just like with normal functions.'}</p>
                <p>{'The first next(..) call, we don\'t send in anything. Why? Because there\'s no yield expression to receive what we pass in.'}</p>
                <p>{'But if we did pass in a value to that first next(..) call, nothing bad would happen. It would just be a tossed-away value. ES6 says for generator functions to ignore the unused value in this case. (Note: At the time of writing, nightlies of both Chrome and FF are fine, but other browsers may not yet be fully compliant and may incorrectly throw an error in this case)'}</p>
                <p>{'The second next(12) call sends 12 to the first yield (x + 1) expression. The third next(13) call sends 13 to the second yield (y / 3) expression. Re-read that a few times. It\'s weird for most, the first several times they see it.'}</p>

                <h2>Summary</h2>
                <p>{'OK, so that\'s it for the basics of generators. Don\'t worry if it\'s a little mind-bending still. All of us have felt that way at first!'}</p>
                <p>{'It\'s natural to wonder what this new exotic toy is going to do practically for your code. There\'s a lot more to them, though. We\'ve just scratched the surface. So we have to dive deeper before we can discover just how powerful they can/will be.'}</p>

            </TopicPageWrap>
        );
    }
});

module.exports = Topic;