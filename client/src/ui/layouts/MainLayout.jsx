/**
 * @jsx React.DOM
 */
'use strict';

var React = require('react');
var Header = require('../components/Header.jsx');

require('./MainLayout.css');

var MainLayout = React.createClass({
    propTypes: {
        pubsub: React.PropTypes.object,
    },

    render: function() {
        return (
            <div className="MainLayout">
                <Header pubsub={this.props.pubsub} />
                <div className="content">
                    {this.props.children}
                </div>
            </div>
        );
    }
});

module.exports = MainLayout;