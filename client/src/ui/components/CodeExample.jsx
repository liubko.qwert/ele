/**
 * @jsx React.DOM
 */
'use strict';

var React = require('react');

var CodeExample = React.createClass({
    propTypes: {
        data: React.PropTypes.string,
    },

    render() {
        var html = prettyPrintOne(this.props.data);
        return (
            <pre className="CodeExample">
                <code className="prettyprint language-js"
                      dangerouslySetInnerHTML={{__html: html}} />
            </pre>
        );
    }
});

module.exports = CodeExample;