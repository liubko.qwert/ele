/**
 * @jsx React.DOM
 */
'use strict';

var React = require('react');
var cx = require('react-classset');

var Button = React.createClass({
    propTypes: {
        value: React.PropTypes.string,
        onClick: React.PropTypes.func,
        size: React.PropTypes.oneOf(['large']),
        type: React.PropTypes.oneOf(['primary','success']),
    },

    handleClick() {
        if (this.props.onClick) {
            this.props.onClick();
        }
    },

    render() {
        var btnClass = cx({
            'Button': true,
            'btn': true,
            'btn-success': this.props.type=='success',
            'btn-primary': this.props.type=='primary',
            'btn-lg': this.props.size=='large'
        });

        return (
            <button type='button' className={btnClass} onClick={this.handleClick}>
                {this.props.value}
            </button>
        );
    }
});

module.exports = Button;