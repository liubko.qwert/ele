/**
 * @jsx React.DOM
 */
'use strict';

var React = require('react');

// require('./Header.css');
var Header = React.createClass({
    propTypes: {
        pubsub: React.PropTypes.object.isRequired,
    },

    handleGoHome() {
        this.props.pubsub.publish('showHomePage');
    },

    render() {
        return (
            <div className="navbar navbar-inverse navbar-fixed-top" role="navigation">
              <div className="container">
                <div className="navbar-header">
                  <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span className="sr-only">Toggle navigation</span>
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                  </button>
                  <a className="navbar-brand" onClick={this.handleGoHome}>Study WEB</a>
                </div>
                <div className="collapse navbar-collapse">
                  <ul className="nav navbar-nav">
                    <li className="active">
                        <a onClick={this.handleGoHome}>Home</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
        );
    }
});

module.exports = Header;