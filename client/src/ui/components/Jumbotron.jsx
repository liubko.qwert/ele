/**
 * @jsx React.DOM
 */
'use strict';

var React = require('react');

var Jumbotron = React.createClass({
    render() {
        return (
            <div className="jumbotron">
              <div className="container">
                <h1>Welcome</h1>
                <p>Select course and start learning WEB now</p>
                <blockquote className="blockquote-reverse">
                    <footer>You can learn anything</footer>
                </blockquote>
              </div>
            </div>
        );
    }
});

module.exports = Jumbotron;