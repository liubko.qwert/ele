/**
 * @jsx React.DOM
 */
'use strict';

var React = require('react');
var Button = require('./Button.jsx');

require('./TopicListView.css');

var TopicListView = React.createClass({
    propTypes: {
        title: React.PropTypes.string.isRequired,
        description: React.PropTypes.string,
        quizResult: React.PropTypes.string,
        onClick: React.PropTypes.func.isRequired,
        showBtn: React.PropTypes.bool.isRequired,
    },

    render() {
        var badgeNode;
        if (this.props.quizResult) {
            badgeNode = (<span className="badge pull-right">{this.props.quizResult}/5</span>);
        }

        var btnNode = this.props.showBtn
            ? ( <Button value="Study"
                        type="success"
                        onClick={this.props.onClick} /> )
            : ( <Button value="Coming soon"
                        type="primary" /> );

        return (
            <div className="TopicListView col-md-4">
                <h2>{this.props.title} {badgeNode}</h2>
                <p>{this.props.description}</p>
                <p> {btnNode} </p>
            </div>
        );
    }
});

module.exports = TopicListView;