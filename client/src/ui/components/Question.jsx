/**
 * @jsx React.DOM
 */
'use strict';

var React = require('react');
// var cx = require('react-classset');
var _ = require('lodash');
var $ = require('jquery');

var RadioButtonGroup = React.createClass({
    propTypes: {
        data: React.PropTypes.object,
        onClick: React.PropTypes.func,
    },

    getInitialState(){
        return {
            selectedID: null
        };
    },

    handleClick(id) {
        this.setState({
            selectedID: id
        });
    },

    getAnswer() {
        return $(this.refs.root.getDOMNode()).find('input:checked').val();
    },

    render() {
        var choisesNode = _.map(this.props.data.choises, choise =>
            <div className="radio" key={choise.id}>
              <label>
                <input type="radio" name={'question_'+this.props.data.id} value={choise.id} />
                {choise.text}
              </label>
            </div>
        );

        return (
            <li ref="root" classNam='RadioButtonGroup'>
                <p>{this.props.data.question}</p>
                <div>
                    {choisesNode}
                </div>
            </li>

        );
    }
});

module.exports = RadioButtonGroup;