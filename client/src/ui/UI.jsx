/**
 * @jsx React.DOM
 */
'use strict';

var React = require('react');

var Layout = require('./layouts/MainLayout.jsx');

var PAGES = {
    'home': require('./pages/HomePage.jsx'),
    'topic-1': require('./pages/TopicES6.jsx'),
    'topic-2': require('./pages/TopicGenerators.jsx'),
    'quiz': require('./pages/QuizPage.jsx'),
    'quizOverview': require('./pages/QuizOverview.jsx'),
};

var UI = React.createClass({
    propTypes: {
        pubsub: React.PropTypes.object.isRequired
    },

    getInitialState: function() {
        return {
            page: 'home',
            pageData: {}
        };
    },

    render: function() {
        var pageNode;
        if (this.state.page) {
            var page = PAGES[this.state.page];
            pageNode = (
                <Layout pubsub={this.props.pubsub}>
                    <page data={this.state.pageData} pubsub={this.props.pubsub} />
                </Layout>
            );
        }

        return (
            <div className="UI">
                {pageNode}
            </div>
        );
    }
});

module.exports = UI;