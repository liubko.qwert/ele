'use strict';
var React = require('react');

class App {
    constructor(args) {
        if (!args.ui) throw '"ui" required';
        if (!args.pubsub) throw '"pubsub" required';
        if (!args.repository) throw '"repository" required';
        if (!args.presenter) throw '"presenter" required';

        this.ui = args.ui;
        this.pubsub = args.pubsub;
        this.repository = args.repository;
        this.presenter = args.presenter;
    }

    run() {
        this.initPubSubSubscriptions();
        this.initialRender();
    }

    initialRender() {
        React.renderComponent(this.ui, document.getElementById('content'));
        this.presenter.showHomePage();
    }

    initPubSubSubscriptions() {
        this.pubsub.subscribe('selectTopic', (msg, data) =>
            this.presenter.selectTopic(data)
        );

        this.pubsub.subscribe('startQuiz', (msg, data) =>
            this.presenter.startQuiz(data)
        );

        this.pubsub.subscribe('showHomePage', () =>
            this.presenter.showHomePage()
        );

        this.pubsub.subscribe('submitAnswers', (msg, data) =>
            this.presenter.submitAnswers(data)
        );


    }
}

module.exports = App;