'use strict';

var $ = require('jquery');
var AppFactory = require('./AppFactory');

$(document).ready(() => {
    var app = new AppFactory().create();
    app.run();

    window.app = app;
});