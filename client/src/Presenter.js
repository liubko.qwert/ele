'use strict';

var Q = require('q');
var _ = require('lodash');

class Base {
    constructor(args) {
        if (!args.repository) throw "repository required";
        if (!args.ui) throw "ui required";
        if (!args.pubsub) throw "pubsub required";

        this.repository = args.repository;
        this.ui = args.ui;
        this.pubsub = args.pubsub;
    }

    renderPage({page, pageData}) {
        let newState = {};
        if (page) {
            newState.page = page;
        }
        if (pageData) {
            newState.pageData = pageData;
        }

        this.ui.setState(newState);
    }
}

class Presenter extends Base {
    selectTopic({topicID}) {
        this.renderPage({
            page: topicID
        });
    }

    startQuiz({topicID}) {
        this.repository.getQuiz(topicID)
            .then(quizData =>
                this.renderPage({
                    page: 'quiz',
                    pageData: {
                        topicID: topicID,
                        quizData: quizData,

                    }
                })
            )
            .catch(err => console.error('[Presenter.startQuiz]',err));
    }

    showHomePage() {
        Q([this.repository.getAllTopics(),
            this.repository.getAllQuizResults()])
            .spread( (topics, quizResults) => {
                topics = _.map(topics, topic => {
                    var resultData = _.where(quizResults, {topicID: topic.id})[0];
                    topic.quizResult = resultData ? resultData.result : null;
                    return topic;
                });

                this.renderPage({
                    page: 'home',
                    pageData: {
                        topics: topics
                    }
                });
            })
            .catch(err => console.error('[Presenter.showHomePage]',err));

    }

    submitAnswers({topicID, userAnswers}) {
        this.repository.submitAnswers(topicID, userAnswers)
            .then(quizResult =>
                this.renderPage({
                    page: 'quizOverview',
                    pageData: {
                        topicID: topicID,
                        quizResult: quizResult,
                    }
                })
            )
            .catch(err => console.error('[Presenter.submitAnswers]',err));
    }
}

module.exports = Presenter;