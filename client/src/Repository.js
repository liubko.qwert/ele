'use strict';

var Q = require('q');
var _ = require('lodash');
// var UserAgent = require('./UserAgent');
var quizes = require('./etc/quiz.json').quizes;
var topics = require('./etc/topic.json').topics;

class Repository {
    constructor() {
        // this.ua = new UserAgent();
    }

    getQuiz(topicID) {
        return Q(
            _.where(quizes, {id:topicID})[0]
        );
    }

    submitAnswers(topicID, userResults) {
        var quizData = _.where(quizes, {id:topicID})[0];

        var rightAnswersCounter = 0;
        var results = _.map(userResults, userQuestionData => {
            var questionData = _.where(quizData.questions, {id: userQuestionData.questionID})[0];
            var rightAnswer = _.where(questionData.choises, {id: questionData.answer})[0];
            var userAnswer = _.where(questionData.choises, {id: parseInt(userQuestionData.answer)})[0];
            var isRight = parseInt(questionData.answer) == parseInt(userQuestionData.answer);

            if (isRight) {
                rightAnswersCounter++;
            }

            return {
                isRight: isRight,
                question: questionData.question,
                userAnswer: userAnswer ? userAnswer.text : 'Not provided',
                rightAnswer: rightAnswer.text,
                answerExplanation: questionData.explanation
            };
        });


        localStorage.setItem('quizResults/'+topicID, JSON.stringify(rightAnswersCounter));
        // myValue = localStorage.getItem('myKey');

        return Q(results);
    }

    getAllQuizResults() {
        var ids = _.pluck(quizes, 'id');
        return Q( _.map(ids, id => {
            return {
                topicID: id,
                result: localStorage.getItem('quizResults/'+id)
            };
        }));
    }

    getAllTopics() {
        return Q( topics );
    }
}

module.exports = Repository;